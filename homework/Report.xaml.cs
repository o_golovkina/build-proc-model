﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace homework
{
    /// <summary>
    /// Логика взаимодействия для Report.xaml
    /// </summary>
    public partial class Report : Window
    {
        List<Database> database;
        List<Timetable> timetables = new List<Timetable>();

        public Report(List<Database> database)
        {
            InitializeComponent();

            this.database = database;

            if (database[0].Locksmith3 == null)
                Profit.Content = Math.Round(MainWindow.SumMoneyBrigadeL6, 2).ToString();
            else
                Profit.Content = Math.Round(MainWindow.SumMoneyBrigadeL3L6, 2).ToString();

            int i = 1;

            string BuldWorkShedule, BuldWaitShedule, BuldRepairShedule, ExcWorkShedule, ExcWaitShedule, ExcRepairShedule;

            double res = 0;
            foreach (var db in database) 
            {
                BuldRepairShedule = BuldWorkShedule = BuldWaitShedule = ExcWaitShedule = ExcWorkShedule = ExcRepairShedule ="";
                double resDay = 0;
                foreach (var buld_sh in db.Buldozer.Shedule)
                {
                    if (buld_sh[2] == (int)MainWindow.WorkMachinesState.WORK)
                    {
                        var time1 = TimeSpan.FromMinutes(buld_sh[0]);
                        BuldWorkShedule += string.Format($"{time1.ToString(@"hh\:mm")} - ");
                        var time2 = TimeSpan.FromMinutes(buld_sh[1]);
                        BuldWorkShedule += string.Format($"{time2.ToString(@"hh\:mm")}\n");
                        Debug.WriteLine($"Часы Б {(time2 - time1).TotalHours}");
                        resDay += (time2 - time1).TotalHours * 3000;
                    }
                    if (buld_sh[2] == (int)MainWindow.WorkMachinesState.PAUSE)
                    {
                        var time1 = TimeSpan.FromMinutes(buld_sh[0]);
                        BuldWaitShedule += string.Format($"{time1.ToString(@"hh\:mm")} - ");
                        var time2 = TimeSpan.FromMinutes(buld_sh[1]);
                        BuldWaitShedule += string.Format($"{time2.ToString(@"hh\:mm")} - ");
                        resDay -= (time2 - time1).TotalHours * 3000;
                    }
                    if (buld_sh[2] == (int)MainWindow.WorkMachinesState.REPAIR)
                    {
                        var time1 = TimeSpan.FromMinutes(buld_sh[0]);
                        BuldRepairShedule += string.Format($"{time1.ToString(@"hh\:mm")} - ");
                        var time2 = TimeSpan.FromMinutes(buld_sh[1]);
                        BuldRepairShedule += string.Format($"{time2.ToString(@"hh\:mm")}\n");
                    }
                }
                Debug.WriteLine($"Б {resDay}");
                res += resDay;

                resDay = 0;
                foreach (var exc_sh in db.Excavator.Shedule)
                {

                    if (exc_sh[2] == (int)MainWindow.WorkMachinesState.WORK)
                    {
                        var time1 = TimeSpan.FromMinutes(exc_sh[0]);
                        ExcWorkShedule += string.Format($"{time1.ToString(@"hh\:mm")} - ");
                        var time2 = TimeSpan.FromMinutes(exc_sh[1]);
                        ExcWorkShedule += string.Format($"{time2.ToString(@"hh\:mm")}\n");
                        Debug.WriteLine($"Часы Э {(time2 - time1).TotalHours}");
                        resDay += (time2 - time1).TotalHours * 5000;
                    }
                    if (exc_sh[2] == (int)MainWindow.WorkMachinesState.PAUSE)
                    {
                        var time1 = TimeSpan.FromMinutes(exc_sh[0]);
                        ExcWaitShedule += string.Format($"{time1.ToString(@"hh\:mm")} - ");
                        var time2 = TimeSpan.FromMinutes(exc_sh[1]);
                        ExcWaitShedule += string.Format($"{time2.ToString(@"hh\:mm")}\n");
                        resDay -= (time2 - time1).TotalHours * 5000;
                    }
                    if (exc_sh[2] == (int)MainWindow.WorkMachinesState.REPAIR)
                    {
                        var time1 = TimeSpan.FromMinutes(exc_sh[0]);
                        ExcRepairShedule += string.Format($"{time1.ToString(@"hh\:mm")} - ");
                        var time2 = TimeSpan.FromMinutes(exc_sh[1]);
                        ExcRepairShedule += string.Format($"{time2.ToString(@"hh\:mm")}\n");
                    }
                }

                Debug.WriteLine($"Э {resDay}");
                res += resDay;

                timetables.Add(new Timetable(i.ToString(), 
                    ExcWorkShedule,
                    ExcWaitShedule,
                    ExcRepairShedule, 
                    BuldWorkShedule, 
                    BuldWaitShedule, 
                    BuldRepairShedule));
                i++;
            }
            SheduleGrid.ItemsSource = timetables;
        }
        private void ShowGraphic(object sender, MouseButtonEventArgs e)
        {
            Graphic graphic = new Graphic(database[SheduleGrid.SelectedIndex], SheduleGrid.SelectedIndex+1);
            graphic.ShowDialog();
        }
        private void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            var displayName = GetPropertyDisplayName(e.PropertyDescriptor);

            if (!string.IsNullOrEmpty(displayName))
            {
                e.Column.Header = displayName;
            }
        }
        public static string GetPropertyDisplayName(object descriptor)
        {
            var pd = descriptor as PropertyDescriptor;

            if (pd != null)
            {
                var displayName = pd.Attributes[typeof(DisplayNameAttribute)] as DisplayNameAttribute;

                if (displayName != null && displayName != DisplayNameAttribute.Default)
                {
                    return displayName.DisplayName;
                }
            }
            else
            {
                var pi = descriptor as PropertyInfo;

                if (pi != null)
                {
                    Object[] attributes = pi.GetCustomAttributes(typeof(DisplayNameAttribute), true);
                    for (int i = 0; i < attributes.Length; ++i)
                    {
                        var displayName = attributes[i] as DisplayNameAttribute;
                        if (displayName != null && displayName != DisplayNameAttribute.Default)
                        {
                            return displayName.DisplayName;
                        }
                    }
                }
            }
            return null;
        }

    }
}
