﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework
{
    public class Machines
    {
        public List<int[]> Shedule;

        public int expected_working_time;
        public int[] cost;
        public int timeLeft;

        public Machines(int expected_value, int[] cost)
        {
            Shedule = new List<int[]>();
            expected_working_time = expected_value;
            this.cost = cost;
            timeLeft = 0;
        }

        public void AddTime(int from, int to, MainWindow.WorkMachinesState type)
        {
            Shedule.Add(new int[3] { from, to, (int)type });
            timeLeft = to;
        }

        public int GetInfo(int time)
        {
            foreach (int[] item in Shedule)
                if (item[0] < time && item[1] > time)
                    return 1;
            return 0;
        }
    }
}
