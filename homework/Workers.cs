﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework
{
    public class Workers
    {
        public List<int[]> Shedule;

        public int costPerHour;
        public int timeLeft;

        public Workers(int cost)
        {
            costPerHour = cost;
            Shedule = new List<int[]>();
            timeLeft = 0;
        }
        public void AddTime(int from, int to, MainWindow.WorkLocksmithesState type)
        {
            Shedule.Add(new int[3] { from, to, (int)type });
            timeLeft = to;
        }

        public int GetInfo(int time)
        {
            foreach (int[] item in Shedule)
                if (item[0] < time && item[1] > time)
                    return 1;
            return 0;
        }


    }
}
