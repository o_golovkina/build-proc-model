﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace homework
{
    /// <summary>
    /// Логика взаимодействия для Graphic.xaml
    /// </summary>
    public partial class Graphic : Window
    {
        public Graphic(Database database, int day)
        {
            InitializeComponent();
            SetGraphic(database);

            this.Title = string.Format($"Отчет за {day} день");

            DayProfitMoney.Content = Math.Round(database.DayProfit, 2);
            DayDecreaseMoney.Content = Math.Round(database.DayDecrease, 2);
            DayNetProfitMoney.Content = Math.Round(database.DayNetProfit, 2);
        }

        private void SetGraphic(Database database) {
            
            double sum = 0;
            double width;

            width = (double)(BuldozerDiagram.Width / (MainWindow.WorkingTime + MainWindow.DiagnosticTime));
            List<double> diap = new List<double>();
           
            foreach (var shedule in database.Buldozer.Shedule)
            {
                Rectangle rectangle = new Rectangle();

                rectangle.Height = BuldozerDiagram.Height;

                rectangle.Width = Math.Abs((double)(shedule[1] - shedule[0] + 1) * width );

                if (shedule[2] == (int)MainWindow.WorkMachinesState.WORK)
                {
                    diap.Add(Math.Abs(sum - rectangle.Width));
                    rectangle.Fill = new SolidColorBrush(Colors.LimeGreen);
                }
                else if (shedule[2] == (int)MainWindow.WorkMachinesState.PAUSE)
                    rectangle.Fill = new SolidColorBrush(Colors.Yellow);
                else if (shedule[2] == (int)MainWindow.WorkMachinesState.REPAIR)
                {
                    Debug.WriteLine("Бульдозер");
                    Debug.WriteLine(shedule[0]);
                    rectangle.Fill = new SolidColorBrush(Colors.Red);
                    diap.Add(sum);
                }

                BuldozerDiagram.Children.Add(rectangle);
                Canvas.SetLeft(rectangle, width * shedule[0]);

                sum += rectangle.Width;
            }

            sum = 0;

            foreach (var shedule in database.Excavator.Shedule)
            {
                Rectangle rectangle = new Rectangle();

                rectangle.Height = ExcavatorDiagram.Height;
                rectangle.Width = Math.Abs((double)(shedule[1] - shedule[0] + 1) * width);

                if (shedule[2] == (int)MainWindow.WorkMachinesState.WORK) { 
                    rectangle.Fill = new SolidColorBrush(Colors.LimeGreen);
                }
                else if (shedule[2] == (int)MainWindow.WorkMachinesState.PAUSE)
                    rectangle.Fill = new SolidColorBrush(Colors.Yellow);
                else if (shedule[2] == (int)MainWindow.WorkMachinesState.REPAIR)
                {
                    rectangle.Fill = new SolidColorBrush(Colors.Red);
                }

                Canvas.SetLeft(rectangle, width * shedule[0]);

                ExcavatorDiagram.Children.Add(rectangle);
                sum += rectangle.Width;
            }

            sum = 0;
            int i = 0;
            foreach (var shedule in database.Locksmith6.Shedule)
            {
                Rectangle rectangle = new Rectangle();

                rectangle.Height = Slesar6Diagram.Height;
                rectangle.Width = Math.Abs((double)(shedule[1] - shedule[0] + 1) * width);

                Debug.WriteLine((double)rectangle.Width);

                if (shedule[2] == (int)MainWindow.WorkLocksmithesState.BUSY)
                {
                    Debug.WriteLine("Слесарь");
                    Debug.WriteLine(shedule[0]);
                    rectangle.Fill = new SolidColorBrush(Colors.LimeGreen);
                }
                else if (shedule[2] == (int)MainWindow.WorkLocksmithesState.FREE)
                    rectangle.Fill = new SolidColorBrush(Colors.Transparent);

                Canvas.SetLeft(rectangle, width * shedule[0]);
                Slesar6Diagram.Children.Add(rectangle);
                sum += rectangle.Width;
                i++;
            }

            if (database.Locksmith3 != null)
            {
                foreach (var shedule in database.Locksmith3.Shedule)
                {
                    Rectangle rectangle = new Rectangle();

                    Debug.WriteLine($"{shedule[0]} - {shedule[1]}");

                    rectangle.Height = Slesar3Diagram.Height;
                    rectangle.Width = Math.Abs((double)(shedule[1] - shedule[0] + 1) * width);
                    Debug.WriteLine((double)rectangle.Width);

                    if (shedule[2] == (int)MainWindow.WorkLocksmithesState.BUSY)
                        rectangle.Fill = new SolidColorBrush(Colors.LimeGreen);
                    else if (shedule[2] == (int)MainWindow.WorkLocksmithesState.FREE)
                        rectangle.Fill = new SolidColorBrush(Colors.LightBlue);

                    Canvas.SetLeft(rectangle, width * shedule[0]);
                    Slesar3Diagram.Children.Add(rectangle);
                    sum += rectangle.Width;
                }
            }
        }
    }
}
