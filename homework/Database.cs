﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework
{
    public class Database
    {
        public Workers Locksmith3;
        public Workers Locksmith6;
        public Machines Buldozer;
        public Machines Excavator;
        public double DayProfit, DayDecrease, DayNetProfit;

        public Database(Workers[] l, Machines b, Machines e, double[] money)
        {
            if (l.Count() == 1)
                Locksmith6 = l[0];
            else
            {
                Locksmith6 = l[0];
                Locksmith3 = l[1];
            }
            Buldozer = b;
            Excavator = e;

            DayProfit = money[0];
            DayDecrease = money[1];
            DayNetProfit = money[2];
        }
    }
}
