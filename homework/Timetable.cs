﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework
{
    public class Timetable
    {
        [DisplayName("№ Дня")]
        public string NumDay { get; set; }
        [DisplayName("График работы")]
        public string GraphicWorkExcavator { get; set; }
        [DisplayName("График застоя")]
        public string GraphicPauseExcavator { get; set; }
        [DisplayName("График ремонта")]
        public string GraphicRepairExcavator { get; set; }
        [DisplayName("График работы")]
        public string GraphicWorkBuldozer { get; set; }
        [DisplayName("График застоя")]
        public string GraphicPauseBuldozer { get; set; }
        [DisplayName("График ремонта")]
        public string GraphicRepairBuldozer { get; set; }

        public Timetable(string numDay, 
            string graphicWorkExcavator, 
            string graphicPauseExcavator,
            string graphicRepairExcavator, 
            string graphicWorkBuldozer, 
            string graphicPauseBuldozer,
            string graphicRepairBuldozer)
        {
            NumDay = numDay;
            GraphicWorkExcavator = graphicWorkExcavator;
            GraphicPauseExcavator = graphicPauseExcavator;
            GraphicRepairExcavator = graphicRepairExcavator;
            GraphicWorkBuldozer = graphicWorkBuldozer;
            GraphicPauseBuldozer = graphicPauseBuldozer;
            GraphicRepairBuldozer = graphicRepairBuldozer;
        }
    }
}
