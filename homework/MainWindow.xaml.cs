﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;
using System.Text.RegularExpressions;
using System.Globalization;

namespace homework
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public enum WorkMachinesState { WORK, PAUSE, REPAIR };
        public enum WorkLocksmithesState { FREE, BUSY };
        public enum Brigade { LOCKSMITH3_AND_LOCKSMITH6, ONLY_LOCKSMITH6};

        public List<Database> databaseL3L6 = new List<Database>();
        public List<Database> databaseL6 = new List<Database>();

        public static int WorkingTime, DiagnosticTime;

        public MainWindow()
        {
            InitializeComponent();
        }
        public static double SumMoneyBrigadeL3L6;
        public static double SumMoneyBrigadeL6;

        public double ExpRandomizer(double expected_value)
        {
            Random random = new Random();
            double result = 60 * expected_value * Math.Log(1 / (1 - random.NextDouble()));
            Thread.Sleep(1);
            return result;
        }

        public void Calculate(object sender, RoutedEventArgs e)
        {
            Cursor = Cursors.Wait;

            databaseL3L6.Clear();
            databaseL6.Clear();

            bool state = CheckBoxLocksmith3.IsChecked.Value & CheckBoxLocksmith6.IsChecked.Value;
            WorkingTime = Convert.ToInt32(MachinesWorkingTime.Text) * 60;
            DiagnosticTime = Convert.ToInt32(MachinesPreventionTime.Text) * 60;

            try
            {
                switch (state)
                {
                    case true:
                        CalculateMoney(Brigade.LOCKSMITH3_AND_LOCKSMITH6);
                        break;
                    default:
                        CalculateMoney(Brigade.ONLY_LOCKSMITH6);
                        break;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show($"Внимание! Ошибка ввода данных: {exc.Message}");
            }

            Cursor = Cursors.Arrow;

            CreateReportButton.IsEnabled = true;
        }

        public void CalculateMoney(Brigade brigade)
        {
            if (brigade == Brigade.LOCKSMITH3_AND_LOCKSMITH6)
            {
                SumMoneyBrigadeL3L6 = CalculateTwoLocksmithesMoney();
            }
            else
            {
                SumMoneyBrigadeL6 = CalculateOneLocksmithMoney();
            }

        }


        int j;

        public double CalculateTwoLocksmithesMoney()
        {
            int DaysCount = Convert.ToInt32(TextBoxDaysCount.Text);
            double result = 0;

            j = 1;
            for (int i = 0; i < DaysCount; i++) 
                result += CreateGraphicL3L6();

            return result;
        }

        private double CreateGraphicL3L6()
        {
            double resultProfit = 0;

            int[] cost = { Convert.ToInt32(ProfitBuldozer.Text), Convert.ToInt32(DecreaseExcavator1.Text) };
            Machines buldozer = new Machines(Convert.ToInt32(ExpectedTimeBuldozer.Text), cost);

            int [] cost1 = { Convert.ToInt32(ProfitExcavator.Text), Convert.ToInt32(DecreaseExcavator.Text) };

            Machines excavator = new Machines(Convert.ToInt32(ExpectedTimeExcavator.Text), cost1);
            Workers Locksmith6 = new Workers(Convert.ToInt32(TextBoxSalaryL6.Text));
            Workers Locksmith3 = new Workers(Convert.ToInt32(TextBoxSalaryL3.Text));

            while (true)
            {
                if (buldozer.timeLeft >= WorkingTime && excavator.timeLeft >= WorkingTime)
                    break;

                CalculateTimes(buldozer, excavator, Locksmith6, Locksmith3);
            }

            NormalizeTime(buldozer, excavator, Locksmith6, Locksmith3);

            excavator.AddTime(WorkingTime + 1, WorkingTime + DiagnosticTime, WorkMachinesState.REPAIR);
            buldozer.AddTime(WorkingTime + 1, WorkingTime + DiagnosticTime, WorkMachinesState.REPAIR);

            resultProfit = CalculateProfit(resultProfit, buldozer, excavator, Locksmith6, Locksmith3);
            Debug.WriteLine(resultProfit);

            return resultProfit;
        }

        private double CalculateProfit(double resultProfit, Machines buldozer, Machines excavator, Workers Locksmith6, Workers Locksmith3)
        {
            double DayProfit, DayDecrease;

            DayProfit = DayDecrease = 0;

            double sumWork = 0;
            double sumWait = 0;
            double resProfit = 0;

            foreach (int[] item in buldozer.Shedule)
                if (buldozer.Shedule.IndexOf(item) != buldozer.Shedule.Count - 1)
                {
                    if (item[2] == (int)WorkMachinesState.WORK)
                        sumWork += (double)((item[1] - item[0]) / 60);
                    if (item[2] == (int)WorkMachinesState.PAUSE)
                        sumWait += (double)((item[1] - item[0]) / 60);
                }

            DayProfit = resProfit += sumWork * buldozer.cost[0];
            resProfit -= sumWait * buldozer.cost[1];
            DayDecrease += sumWait * buldozer.cost[1];

            sumWork = 0;
            sumWait = 0;

            foreach (int[] item in excavator.Shedule)
                if (excavator.Shedule.IndexOf(item) != excavator.Shedule.Count - 1)
                {
                    if (item[2] == (int)WorkMachinesState.WORK)
                        sumWork += (double)((item[1] - item[0]) / 60);
                    if (item[2] == (int)WorkMachinesState.PAUSE)
                        sumWait += (double)((item[1] - item[0]) / 60);
                }

            resProfit += sumWork * excavator.cost[0];
            DayProfit += sumWork * excavator.cost[0];
            resProfit -= sumWait * excavator.cost[1];
            DayDecrease += sumWait * excavator.cost[1];

            foreach (int[] item in Locksmith3.Shedule)
            {
                resProfit -= (item[1] - item[0]) * Locksmith3.costPerHour / 60;
                DayDecrease += (item[1] - item[0]) * Locksmith3.costPerHour / 60;
            }

            foreach (int[] item in Locksmith6.Shedule)
            {
                DayDecrease += (item[1] - item[0]) * Locksmith6.costPerHour / 60;
                resProfit -= (item[1] - item[0]) * Locksmith6.costPerHour / 60;
            }

            resProfit -= 16 * Convert.ToInt32(Waybill.Text);
            DayDecrease += 16 * Convert.ToInt32(Waybill.Text);
            resultProfit = resProfit;

            Workers[] workers = { Locksmith6, Locksmith3 };
            double[] money = { DayProfit, DayDecrease, resProfit };

            databaseL3L6.Add(new Database(workers, buldozer, excavator, money));

            return resultProfit;
        }

        private void CalculateTimes(Machines buldozer, Machines excavator, Workers Locksmith6, Workers Locksmith3)
        {
            int endTimeBuld = buldozer.timeLeft + Convert.ToInt32(Math.Round(ExpRandomizer(buldozer.expected_working_time)));
            int endTimeExc = excavator.timeLeft + Convert.ToInt32(Math.Round(ExpRandomizer(excavator.expected_working_time)));

            buldozer.AddTime(buldozer.timeLeft, buldozer.timeLeft + endTimeBuld, WorkMachinesState.WORK);
            excavator.AddTime(excavator.timeLeft, excavator.timeLeft + endTimeExc, WorkMachinesState.WORK);

            if (excavator.timeLeft <= buldozer.timeLeft)
            {
                int ExpectedTime = excavator.timeLeft + 1;
                int ExpectedRandomTime = Convert.ToInt32(Math.Round(ExpRandomizer(Convert.ToDouble(ExpectedTimeWorkingL6L3Exc.Text, CultureInfo.InvariantCulture))));

                excavator.AddTime(ExpectedTime, ExpectedRandomTime + ExpectedTime, WorkMachinesState.REPAIR);
                Locksmith6.AddTime(ExpectedTime, ExpectedRandomTime + ExpectedTime, WorkLocksmithesState.BUSY);
                Locksmith3.AddTime(ExpectedTime, ExpectedRandomTime + ExpectedTime, WorkLocksmithesState.BUSY);


                if (Locksmith6.GetInfo(buldozer.timeLeft + 1) == (int)WorkLocksmithesState.BUSY)
                {
                    buldozer.AddTime(buldozer.timeLeft + 1, excavator.timeLeft, WorkMachinesState.PAUSE);
                    ExpectedTime = buldozer.timeLeft + 1;
                    ExpectedRandomTime = Convert.ToInt32(Math.Round(ExpRandomizer(Convert.ToDouble(ExpectedTimeWorkingL6L3Bul.Text, CultureInfo.InvariantCulture))));

                    buldozer.AddTime(ExpectedTime, ExpectedRandomTime + ExpectedTime, WorkMachinesState.REPAIR);
                    Locksmith6.AddTime(ExpectedTime, ExpectedRandomTime + ExpectedTime, WorkLocksmithesState.BUSY);
                    Locksmith3.AddTime(ExpectedTime, ExpectedRandomTime + ExpectedTime, WorkLocksmithesState.BUSY);
                }
            }
            else
            {
                int BuldozerExpectedTime = buldozer.timeLeft + 1;
                int BuldozerRandomTime = Convert.ToInt32(Math.Round(ExpRandomizer(Convert.ToDouble(ExpectedTimeWorkingL6L3Bul.Text, CultureInfo.InvariantCulture))));

                buldozer.AddTime(BuldozerExpectedTime, BuldozerExpectedTime + BuldozerRandomTime, WorkMachinesState.REPAIR);
                Locksmith6.AddTime(BuldozerExpectedTime, BuldozerExpectedTime + BuldozerRandomTime, WorkLocksmithesState.BUSY);
                Locksmith3.AddTime(BuldozerExpectedTime, BuldozerExpectedTime + BuldozerRandomTime, WorkLocksmithesState.BUSY);

                if (Locksmith6.GetInfo(excavator.timeLeft + 1) == (int)WorkLocksmithesState.BUSY)
                {
                    excavator.AddTime(BuldozerExpectedTime, buldozer.timeLeft, WorkMachinesState.PAUSE);
                    int ExcavatorExpectedTime = excavator.timeLeft + 1;
                    int ExcavatorRandomTime = Convert.ToInt32(Math.Round(ExpRandomizer(Convert.ToInt32(ExpectedTimeWorkingL6Exc.Text))));

                    excavator.AddTime(ExcavatorExpectedTime, ExcavatorRandomTime + ExcavatorExpectedTime, WorkMachinesState.REPAIR);
                    Locksmith6.AddTime(ExcavatorExpectedTime, ExcavatorRandomTime + ExcavatorExpectedTime, WorkLocksmithesState.BUSY);
                    Locksmith3.AddTime(ExcavatorExpectedTime, ExcavatorRandomTime + ExcavatorExpectedTime, WorkLocksmithesState.BUSY);
                }
            }

        }

        private void NormalizeTime(Machines buldozer, Machines excavator, Workers Locksmith6, Workers Locksmith3)
        {
            for (int i = 0; i < buldozer.Shedule.Count; i++)
                if (buldozer.Shedule[i][0] <= WorkingTime && buldozer.Shedule[i][1] >= WorkingTime)
                {
                    buldozer.Shedule[i][1] = WorkingTime;
                    buldozer.Shedule.RemoveRange(i + 1, buldozer.Shedule.Count - i - 1);
                    break;
                }

            for (int i = 0; i < excavator.Shedule.Count; i++)
                if (excavator.Shedule[i][0] <= WorkingTime && excavator.Shedule[i][1] >= WorkingTime)
                {
                    excavator.Shedule[i][1] = WorkingTime;
                    excavator.Shedule.RemoveRange(i + 1, excavator.Shedule.Count - i - 1);
                    break;
                }

            for (int i = 0; i < Locksmith3.Shedule.Count; i++)
                if (Locksmith3.Shedule[i][0] >= WorkingTime && Locksmith3.Shedule[i][1] >= WorkingTime)
                {
                    Locksmith3.Shedule.RemoveRange(i, Locksmith3.Shedule.Count - i);
                    break;
                }
                else if (Locksmith3.Shedule[i][0] <= WorkingTime && Locksmith3.Shedule[i][1] >= WorkingTime)
                {
                    Locksmith3.Shedule[i][1] = WorkingTime;
                    Locksmith3.Shedule.RemoveRange(i + 1, Locksmith3.Shedule.Count - i - 1);
                    break;
                }

            for (int i = 0; i < Locksmith6.Shedule.Count; i++)
                if (Locksmith6.Shedule[i][0] >= WorkingTime && Locksmith6.Shedule[i][1] >= WorkingTime)
                {
                    Locksmith6.Shedule.RemoveRange(i, Locksmith6.Shedule.Count - i);
                    break;
                }
                else if (Locksmith6.Shedule[i][0] <= WorkingTime && Locksmith6.Shedule[i][1] >= WorkingTime)
                {
                    Locksmith6.Shedule[i][1] = WorkingTime;
                    Locksmith6.Shedule.RemoveRange(i + 1, Locksmith6.Shedule.Count - i - 1);
                    break;
                }
        }

        private double CalculateOneLocksmithMoney()
        {
            int DaysCount = Convert.ToInt32(TextBoxDaysCount.Text);
            double result = 0;

            j = 1;

            for (int i = 0; i < DaysCount; i++)
                result += CreateGraphicL6();

            return result;
        }

        private double CreateGraphicL6()
        {
            double resultProfit = 0;

            int[] cost = { Convert.ToInt32(ProfitBuldozer.Text), Convert.ToInt32(DecreaseExcavator1.Text) };
            Machines buldozer = new Machines(Convert.ToInt32(ExpectedTimeBuldozer.Text), cost);

            int[] cost1 = { Convert.ToInt32(ProfitExcavator.Text), Convert.ToInt32(DecreaseExcavator.Text) };
            Machines excavator = new Machines(Convert.ToInt32(ExpectedTimeExcavator.Text), cost1);

            Workers Locksmith6 = new Workers(Convert.ToInt32(TextBoxSalaryL6.Text));

            while (true)
            {
                if (buldozer.timeLeft >= WorkingTime && excavator.timeLeft >= WorkingTime)
                    break;

                CalculateTimes(buldozer, excavator, Locksmith6);
            }

            NormalizeTime(buldozer, excavator, Locksmith6);

            excavator.AddTime(WorkingTime + 1, WorkingTime + DiagnosticTime, WorkMachinesState.REPAIR);
            buldozer.AddTime(WorkingTime + 1, WorkingTime + DiagnosticTime, WorkMachinesState.REPAIR);

            resultProfit = CalculateProfit(resultProfit, buldozer, excavator, Locksmith6);

            Debug.WriteLine(resultProfit);

            return resultProfit;
        }

        private void NormalizeTime(Machines buldozer, Machines excavator, Workers Locksmith6)
        {
            for (int i = 0; i < buldozer.Shedule.Count; i++)
                if (buldozer.Shedule[i][0] <= WorkingTime && buldozer.Shedule[i][1] >= WorkingTime)
                {
                    buldozer.Shedule[i][1] = WorkingTime;
                    buldozer.Shedule.RemoveRange(i + 1, buldozer.Shedule.Count - i - 1);
                    break;
                }

            for (int i = 0; i < excavator.Shedule.Count; i++)
                if (excavator.Shedule[i][0] <= WorkingTime && excavator.Shedule[i][1] >= WorkingTime)
                {
                    excavator.Shedule[i][1] = WorkingTime;
                    excavator.Shedule.RemoveRange(i + 1, excavator.Shedule.Count - i - 1);
                    break;
                }

            for (int i = 0; i < Locksmith6.Shedule.Count; i++)
                if (Locksmith6.Shedule[i][0] >= WorkingTime && Locksmith6.Shedule[i][1] >= WorkingTime)
                {
                    Locksmith6.Shedule.RemoveRange(i, Locksmith6.Shedule.Count - i);
                    break;
                }
                else if (Locksmith6.Shedule[i][0] <= WorkingTime && Locksmith6.Shedule[i][1] >= WorkingTime)
                {
                    Locksmith6.Shedule[i][1] = WorkingTime;
                    Locksmith6.Shedule.RemoveRange(i + 1, Locksmith6.Shedule.Count - i - 1);
                    break;
                }
        }

        private void CalculateTimes(Machines buldozer, Machines excavator, Workers Locksmith6)
        {
            int endTimeBuld = buldozer.timeLeft + Convert.ToInt32(Math.Round(ExpRandomizer(buldozer.expected_working_time)));
            int endTimeExc = excavator.timeLeft + Convert.ToInt32(Math.Round(ExpRandomizer(excavator.expected_working_time)));
            
            buldozer.AddTime(buldozer.timeLeft, buldozer.timeLeft + endTimeBuld, WorkMachinesState.WORK);
            excavator.AddTime(excavator.timeLeft, excavator.timeLeft + endTimeExc, WorkMachinesState.WORK);
            
            if (excavator.timeLeft <= buldozer.timeLeft)
            {
                int ExpectedTime = excavator.timeLeft + 1;
                int ExpectedRandomTime = Convert.ToInt32(Math.Round(ExpRandomizer(Convert.ToInt32(ExpectedTimeWorkingL6Exc.Text))));

                excavator.AddTime(ExpectedTime, ExpectedRandomTime + ExpectedTime, WorkMachinesState.REPAIR);
                Locksmith6.AddTime(ExpectedTime, ExpectedRandomTime + ExpectedTime, WorkLocksmithesState.BUSY);

                if (Locksmith6.GetInfo(buldozer.timeLeft + 1) == (int)WorkLocksmithesState.BUSY)
                {
                    buldozer.AddTime(buldozer.timeLeft + 1, excavator.timeLeft, WorkMachinesState.PAUSE);
                    ExpectedTime = buldozer.timeLeft + 1;
                    ExpectedRandomTime = Convert.ToInt32(Math.Round(ExpRandomizer(Convert.ToInt32(ExpectedTimeWorkingL6Bul.Text))));

                    buldozer.AddTime(ExpectedTime, ExpectedRandomTime + ExpectedTime, WorkMachinesState.REPAIR);
                    Locksmith6.AddTime(ExpectedTime, ExpectedRandomTime + ExpectedTime, WorkLocksmithesState.BUSY);
                }
            }
            else
            {
                int BuldozerExpectedTime = buldozer.timeLeft + 1;
                int BuldozerRandomTime = Convert.ToInt32(Math.Round(ExpRandomizer(Convert.ToInt32(ExpectedTimeWorkingL6Bul.Text))));

                buldozer.AddTime(BuldozerExpectedTime, BuldozerExpectedTime + BuldozerRandomTime, WorkMachinesState.REPAIR);
                Locksmith6.AddTime(BuldozerExpectedTime, BuldozerExpectedTime + BuldozerRandomTime, WorkLocksmithesState.BUSY);

                if (Locksmith6.GetInfo(excavator.timeLeft + 1) == (int)WorkLocksmithesState.BUSY)
                {
                    excavator.AddTime(BuldozerExpectedTime, buldozer.timeLeft, WorkMachinesState.PAUSE);
                    int ExcavatorExpectedTime = excavator.timeLeft + 1;
                    int ExcavatorRandomTime = Convert.ToInt32(Math.Round(ExpRandomizer(Convert.ToInt32(ExpectedTimeWorkingL6Exc.Text))));

                    excavator.AddTime(ExcavatorExpectedTime, ExcavatorRandomTime + ExcavatorExpectedTime, WorkMachinesState.REPAIR);
                    Locksmith6.AddTime(ExcavatorExpectedTime, ExcavatorRandomTime + ExcavatorExpectedTime, WorkLocksmithesState.BUSY);
                }
            }
        }

        private void CreateReport(object sender, RoutedEventArgs e)
        {
            bool state = CheckBoxLocksmith3.IsChecked.Value & CheckBoxLocksmith6.IsChecked.Value;
            Report report;
            if (!state)
                report = new Report(databaseL6);
            else
                report = new Report(databaseL3L6);
            report.ShowDialog();
        }

        private double CalculateProfit(double resultProfit, Machines buldozer, Machines excavator, Workers Locksmith6)
        {
            double DayProfit, DayDecrease;

            DayProfit = DayDecrease = 0;

            double sumWork = 0;
            double sumWait = 0;
            double resProfit = 0;

            foreach (int[] item in buldozer.Shedule)
                if (buldozer.Shedule.IndexOf(item) != buldozer.Shedule.Count - 1)
                {
                    if (item[2] == (int)WorkMachinesState.WORK)
                        sumWork += (double)((item[1] - item[0]) / 60);
                    if (item[2] == (int)WorkMachinesState.PAUSE)
                        sumWait += (double)((item[1] - item[0]) / 60);
                }

            DayProfit = resProfit += sumWork * buldozer.cost[0];
            resProfit -= sumWait * buldozer.cost[1];
            DayDecrease += sumWait * buldozer.cost[1];

            sumWork = 0;
            sumWait = 0;

            foreach (int[] item in excavator.Shedule)
                if (excavator.Shedule.IndexOf(item) != excavator.Shedule.Count - 1)
                {
                    if (item[2] == (int)WorkMachinesState.WORK)
                        sumWork += (double)((item[1] - item[0]) / 60);
                    if (item[2] == (int)WorkMachinesState.PAUSE)
                        sumWait += (double)((item[1] - item[0]) / 60);
                }

            resProfit += sumWork * excavator.cost[0];
            DayProfit += sumWork * excavator.cost[0];
            resProfit -= sumWait * excavator.cost[1];
            DayDecrease += sumWait * excavator.cost[1];

            foreach (int[] item in Locksmith6.Shedule)
            {
                resProfit -= (item[1] - item[0]) * Locksmith6.costPerHour / 60;
                DayDecrease += (item[1] - item[0]) * Locksmith6.costPerHour / 60;
            }


            resultProfit = resProfit;

            Workers[] workers = { Locksmith6 };

            double[] money = { DayProfit, DayDecrease, resProfit};

            databaseL6.Add(new Database(workers, buldozer, excavator, money));

            return resultProfit;
        }

        private void PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
