﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework
{
    class DataThread
    {
        public int BrigadeState;
        public int DaysCount;
        public DataThread(MainWindow.Brigade brigade, int days)
        {
            BrigadeState = (int)brigade;
            DaysCount = days;
        }
    }
}
